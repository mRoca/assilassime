
all: build-docker-update build-dev start-dev

build-dev:
	docker-compose run --rm ui yarn install --pure-lockfile

build-prod:
	docker-compose run --rm tools yarn install --pure-lockfile --production
	make -s rebuild-prod

rebuild-prod:
	docker-compose run --rm tools npm run-script build
	docker-compose build prod

build-docker-update:
	docker-compose pull

start-dev:
	docker-compose up -d ui
	@echo "Done : http://ui.letshelp.docker/"

start-prod:
	docker-compose up -d --force-recreate prod

logs:
	docker-compose logs -f

stop:
	docker-compose stop

clean: stop
	docker-compose down -v --remove-orphans || true

restart: clean start logs

test-cs:
	docker-compose run --rm ui ./node_modules/.bin/eslint --ext .js --ext .jsx --ignore-path .gitignore --cache app.js ./src

fix-cs:
	docker-compose run --rm ui ./node_modules/.bin/eslint --ext .js --ext .jsx --ignore-path .gitignore --cache --fix ./src

test:
	make test-cs

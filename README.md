# Let's help !

> This project has been created during a Hackathon, this is obviously not the best one you can imagine ;-)


## Install & start dev

```bash
make
```

## Other commands

See the Makefile :-)

### Coding style

See https://github.com/airbnb/javascript/tree/master/react for the UI part coding style,
and https://github.com/airbnb/javascript/blob/master/packages/eslint-config-airbnb/rules/react.js for eslint rules

**Check the cs**

```bash
make test-cs
```

**Fix the cs**

```bash
make fix-cs
```

### Run a command in containers

```bash
docker-compose run --rm ui the_command
```

### Add a package

```bash
docker-compose run --rm ui yarn add package-name

```

module.exports = {
    'extends': ['airbnb'],
    'env': {
        'browser': 1
    },
    'rules': {
        'indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
        'react/jsx-indent': ['error', 4],
        'max-len': ['error', 200],
        "jsx-a11y/href-no-hash": "off",
        "no-plusplus": "off",
    }
};

export const addOrUpdateMarker = (id, marker) => ({
    type: 'ADD_OR_UPDATE_MARKER',
    id,
    marker,
});

export const deleteMarkerByType = markerType => ({
    type: 'DELETE_MARKER_BY_TYPE',
    markerType,
});

export const setMenuIndex = index => ({
    type: 'SET_MENU_INDEX',
    index,
});

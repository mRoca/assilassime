import { LOCATION_CHANGE } from 'react-router-redux';
import { setMenuIndex } from './index';
import { getMenuIndexByRoute } from '../reducers/menuIndex';

const createLocationChangeMiddleware = container => store => next => (action) => {
    next(action);

    if (action.type === LOCATION_CHANGE) {
        const path = action.payload.pathname;

        store.dispatch(setMenuIndex(getMenuIndexByRoute(path)));
    }
};

const getMiddlewares = container => [
    createLocationChangeMiddleware,
].map(fnc => fnc(container));

export default getMiddlewares;

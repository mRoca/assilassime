export const mapRoute = '/map';
export const eventsListRoute = '/events';
export const myProfileRoute = '/myprofile';

const menuRoutes = [
    mapRoute,
    eventsListRoute,
    myProfileRoute,
];

export const menuIndex = (state = '', action) => {
    switch (action.type) {
    case 'SET_MENU_INDEX':
        return action.index;
    default:
        return state;
    }
};

export const getMenuIndexByRoute = route => menuRoutes.indexOf(`/${route.split('/')[1]}`);


import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import markers from './markers';
import { menuIndex } from './menuIndex';

export default combineReducers({
    markers,
    menuIndex,
    routing: routerReducer,
});

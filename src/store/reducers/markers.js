const marker = (state = {}, action) => {
    switch (action.type) {
    case 'ADD_MARKER':
        return {
            id: action.id,
            title: action.marker.title,
            creator: action.marker.creator,
            description: action.marker.description,
            type: action.marker.type,
            position: action.marker.position,
            images: action.marker.images,
        };
    case 'UPDATE_MARKER':
        if (state.id !== action.id) {
            return state;
        }

        return Object.assign({}, state, {
            title: action.marker.title,
            creator: action.marker.creator,
            type: action.marker.type,
            description: action.marker.description,
            position: action.marker.position,
            images: action.marker.images,
        });
    default:
        return state;
    }
};

const markers = (state = [], action) => {
    switch (action.type) {
    case 'ADD_MARKER':
        return [
            ...state,
            marker(undefined, action),
        ];
    case 'UPDATE_MARKER':
        return state.map(t => marker(t, action));
    case 'DELETE_MARKER':
        return state.filter(t => t.id !== action.id);
    case 'DELETE_MARKER_BY_TYPE':
        return state.filter(t => t.type !== action.markerType);
    case 'ADD_OR_UPDATE_MARKER':
        if (state.findIndex(t => t.id === action.id) > -1) {
            return markers(state, Object.assign({}, action, { type: 'UPDATE_MARKER' }));
        }

        return markers(state, Object.assign({}, action, { type: 'ADD_MARKER' }));
    default:
        return state;
    }
};

export default markers;

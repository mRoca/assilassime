import { connect } from 'react-redux';
import Layout from '../../components/Layout/Layout';

const mapStateToProps = state => ({
    currentIndex: state.menuIndex,
});

const LayoutContainer = connect(
    mapStateToProps,
)(Layout);

export default LayoutContainer;

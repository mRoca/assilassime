import { connect } from 'react-redux';
import Map from '../../components/Map/Map';
import { addOrUpdateMarker, deleteMarkerByType } from '../actions';

const mapStateToProps = state => ({
    markers: state.markers,
});

const mapDispatchToProps = dispatch => ({
    addOrUpdateMarker: (id, marker) => {
        dispatch(addOrUpdateMarker(id, marker));
    },
    deleteMarkerByType: (type) => {
        dispatch(deleteMarkerByType(type));
    },
});

const MapContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Map);

export default MapContainer;

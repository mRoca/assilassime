import { connect } from 'react-redux';
import MyProfile from '../../components/Home/MyProfile';

const mapStateToProps = () => ({
    friends: [
        { id: 1, name: 'Brendan Lim' },
        { id: 2, name: 'Eric Hoffman' },
        { id: 3, name: 'Grace Ng' },
        { id: 4, name: 'Kerem Suer' },
        { id: 5, name: 'Raquel Parrado' },
    ],
    events: [
        { id: 1, name: 'Reboucher un trou' },
        { id: 2, name: 'Coup de peinture' },
        { id: 3, name: 'Opération ville propre' },
        { id: 4, name: 'Opération ville propre' },
        { id: 5, name: 'Juste un apéro' },
    ],
});

const MyProfileContainer = connect(
    mapStateToProps,
)(MyProfile);

export default MyProfileContainer;

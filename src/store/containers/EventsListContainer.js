import { connect } from 'react-redux';
import EventsList from '../../components/Events/EventsList';

const mapStateToProps = state => ({
    events: state.markers,
});

const EventsListContainer = connect(
    mapStateToProps,
)(EventsList);

export default EventsListContainer;

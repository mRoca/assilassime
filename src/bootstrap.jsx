import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import LayoutContainer from './store/containers/LayoutContainer';
import Home from './components/Home/Home';
import { mapRoute, eventsListRoute, myProfileRoute } from './store/reducers/menuIndex';
import MapContainer from './store/containers/MapContainer';
import EventsListContainer from './store/containers/EventsListContainer';
import MyProfileContainer from './store/containers/MyProfileContainer';

export default function (store) {
    const history = syncHistoryWithStore(browserHistory, store);

    ReactDOM.render(
        <Provider store={store}>
            <Router history={history}>
                <Route path="/" component={LayoutContainer}>
                    <IndexRoute component={Home} />
                    <Route path={mapRoute} component={MapContainer} />
                    <Route path={eventsListRoute} component={EventsListContainer} />
                    <Route path={myProfileRoute} component={MyProfileContainer} />
                </Route>
            </Router>
        </Provider>,
        document.getElementById('root'),
    );
}

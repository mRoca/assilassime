import { applyMiddleware, createStore } from 'redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import registerServiceWorker from './helpers/registerServiceWorker';
import reducers from './store/reducers';
import bootstrap from './bootstrap';
import getMiddlewares from './store/actions/middlewares';

injectTapEventPlugin(); // See http://www.material-ui.com/#/get-started/installation

const images = [{ url: 'images/trou.jpg' }, { url: 'images/trou2.jpg' }];
const defaultStoreData = {
    markers: [
        { id: 'marker1', type: 'event', position: [6.14, 1.212], title: 'Trou dans la chaussée', creator: 'Michel Roca', date: '2018-06-18', time: '17h45', images },
        { id: 'marker1a', type: 'event', position: [6.141, 1.214], title: 'Plusieurs tas de déchets', creator: 'Michel Roca', date: '2018-06-19', time: '10h00', images },
        { id: 'marker1b', type: 'event', position: [6.138, 1.210], title: 'Nettoyage de la lagune', creator: 'Michel Roca', date: '2018-06-21', time: '17h45', images },
        { id: 'marker1c', type: 'event', position: [6.139, 1.207], title: 'Trou dans la chaussée', creator: 'Michel Roca', date: '2018-06-23', time: '17h45', images },
        { id: 'marker2', type: 'bug', position: [6.141, 1.2125], title: 'Problème d\'évacuation d\'eau', creator: 'Michel Roca', images },
        { id: 'marker2a', type: 'bug', position: [6.1415, 1.2127], title: 'Problème d\'évacuation d\'eau', creator: 'Michel Roca', images },
        { id: 'marker2b', type: 'bug', position: [6.1413, 1.2132], title: 'Problème d\'évacuation d\'eau', creator: 'Michel Roca', images },
        { id: 'marker2c', type: 'bug', position: [6.1409, 1.2131], title: 'Problème d\'évacuation d\'eau', creator: 'Michel Roca', images },
        { id: 'marker3', type: 'town', position: [6.142, 1.21], title: 'Opération nettoyage juin', creator: 'Mairie', date: '2018-06-28', time: '14h00', images },
        { id: 'marker3a', type: 'town', position: [6.1415, 1.2118], title: 'Opération nettoyage juillet', creator: 'Mairie', date: '2018-07-28', time: '14h00', images },
        { id: 'marker3b', type: 'town', position: [6.141, 1.2115], title: 'Opération nettoyage juillet', creator: 'Mairie', date: '2018-07-28', time: '14h00', images },
    ],
    menuIndex: 0,
};

const container = { services: {} }; // Used to inject services in store middlewares. TODO Find another way
const store = createStore(reducers, defaultStoreData, applyMiddleware(...getMiddlewares(container)));

bootstrap(store);

registerServiceWorker();

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Divider, List, ListItem, Subheader } from 'material-ui';
import { ActionAccountCircle, MapsPlace } from 'material-ui/svg-icons/index';
import { grey100, grey800 } from 'material-ui/styles/colors';
import { white } from '../../helpers/style';

class MyProfile extends Component {
    render() {
        return (
            <div>
                <p style={{ color: grey800, textAlign: 'center', backgroundColor: grey100, padding: 20, margin: 0 }}>Mon compte : Michel Roca</p>

                <List style={{ backgroundColor: white }}>
                    <Subheader>Mes coups de main</Subheader>

                    {this.props.events.map(event => (
                        <ListItem
                            key={event.id}
                            primaryText={event.name}
                            leftIcon={<MapsPlace />}
                        />
                    ))}
                </List>
                <Divider />

                <List style={{ backgroundColor: white }}>
                    <Subheader>Mes amis</Subheader>

                    {this.props.friends.map(friend => (
                        <ListItem
                            key={friend.id}
                            primaryText={friend.name}
                            leftIcon={<ActionAccountCircle />}
                        />
                    ))}
                </List>
            </div>
        );
    }
}

MyProfile.propTypes = {
    friends: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        phoneNumer: PropTypes.string,
    })).isRequired,
    events: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
    })).isRequired,
};

export default MyProfile;

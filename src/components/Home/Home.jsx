import React from 'react';

function Home() {
    return (
        <div className="middle">
            <img src="images/hands.png" style={{ height: 60 }} alt="" />

            <span style={{ fontSize: 50 }}>Assilassimé</span>
        </div>
    );
}

export default Home;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { AppBar, BottomNavigation, BottomNavigationItem, IconButton } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { browserHistory } from 'react-router'
import NavigationChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import ActionList from 'material-ui/svg-icons/action/list';
import ActionCheckCircle from 'material-ui/svg-icons/action/check-circle';
import MapsMap from 'material-ui/svg-icons/maps/map';

import './Layout.css';
import { eventsListRoute, mapRoute, myProfileRoute } from '../../store/reducers/menuIndex';
import { orange } from "../../helpers/style";

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: orange,
        primary2Color: orange,
    },
});

const styles = {
    root: {
        maxWidth: 600,
        width: '100%',
        height: '100%',
        margin: 'auto',
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        height: '100%',
        flexGrow: 1,
        overflowX: 'hidden',
        overflowY: 'auto',
        backgroundColor: "#f5f5f5",
        position: 'relative',
    },
    topBar: {
        minHeight: 64,
    },
    bottomBar: {
        alignSelf: 'flex-end',
        minHeight: 64,
        borderTop: '1px solid #ddd'
    },
};

class Layout extends Component {

    navigate = url => browserHistory.push(url);

    render() {
        const previousPage = this.props.location.pathname.replace(new RegExp("/[^/]*$"), "");

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div style={styles.root}>
                    <AppBar
                        title="Assilassimé"
                        iconElementLeft={previousPage !== '' ? <IconButton onClick={() => this.navigate(previousPage)}><NavigationChevronLeft /></IconButton> : <span />}
                        style={styles.topBar}
                    />
                    <div style={styles.content}>
                        {this.props.children}
                    </div>
                    <BottomNavigation selectedIndex={this.props.currentIndex} style={styles.bottomBar}>
                        <BottomNavigationItem
                            label="Carte"
                            icon={<MapsMap />}
                            onClick={() => this.navigate(mapRoute)}
                        />
                        <BottomNavigationItem
                            label="Liste"
                            icon={<ActionList />}
                            onClick={() => this.navigate(eventsListRoute)}
                        />
                        <BottomNavigationItem
                            label="Mes coups de main"
                            icon={<ActionCheckCircle />}
                            onClick={() => this.navigate(myProfileRoute)}
                        />
                    </BottomNavigation>
                </div>
            </MuiThemeProvider>
        );
    }
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
    currentIndex: PropTypes.number.isRequired,
};

export default Layout;

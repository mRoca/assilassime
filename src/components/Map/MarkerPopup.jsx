import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'leaflet';
import { Marker, Popup, PropTypes as MapPropTypes } from 'react-leaflet';
import { Dialog, Snackbar } from 'material-ui';
import Event from '../Events/Event';

class MarkerPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            popupOpened: false,
            snackbarOpened: false,
        };

        this.handlePopupOpen = this.handlePopupOpen.bind(this);
        this.handlePopupClose = this.handlePopupClose.bind(this);
    }

    handlePopupOpen() {
        this.setState({
            popupOpened: true,
        });
    }

    handlePopupClose() {
        this.setState({
            popupOpened: false,
        });
    }

    render() {
        const iconsTypes = {
            new: 'marker-current',
            saved: 'marker-current',
            town: 'marker-town',
            bug: 'marker-bug',
        };
        const icon = new Icon({
            iconUrl: `images/${iconsTypes[this.props.marker.type] || 'marker'}.svg`,
            iconSize: [34, 34],
            iconAnchor: [34, 34],
            popupAnchor: [-17, -34],
        });

        return (
            <Marker position={this.props.marker.position} icon={icon} style={{ color: 'red' }}>
                <Popup onOpen={this.handlePopupOpen}>
                    <div>{this.props.marker.title}</div>
                </Popup>

                <Dialog
                    modal={false}
                    open={this.state.popupOpened}
                    onRequestClose={this.handlePopupClose}
                    autoScrollBodyContent
                >
                    <Event event={this.props.marker} />
                </Dialog>

                <Snackbar
                    open={this.state.snackbarOpened}
                    message="La demande a bien été enregistrée !"
                    autoHideDuration={3000}
                />
            </Marker>
        );
    }
}

MarkerPopup.propTypes = {
    marker: PropTypes.shape({
        type: PropTypes.string.isRequired,
        position: MapPropTypes.latlng.isRequired,
        title: PropTypes.string.isRequired,
    }).isRequired,
};

export default MarkerPopup;

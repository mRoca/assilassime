import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    Circle,
    Map as BaseMap,
    TileLayer,
} from 'react-leaflet';

import { DatePicker, Dialog, RaisedButton, Snackbar, TextField, TimePicker } from 'material-ui';
import MarkersList from './MarkersList';

class Map extends Component {
    constructor(props) {
        super(props);

        this.map = React.createRef();
        this.state = {
            center: [6.14, 1.21],
            currentPosition: [6.14, 1.21],
            currentPositionAccuracy: 200,
            zoom: 16,
            markers: this.props.markers,
            currentMarker: null,
            currentMarkerCreation: {},
            createFormOpened: false,
            createEventFormOpened: false,
            snackbarOpened: false,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleLocationFound = this.handleLocationFound.bind(this);
        this.handleCreateFormClose = this.handleCreateFormClose.bind(this);
        this.openCreateForm = this.openCreateForm.bind(this);
        this.openCreateBugForm = this.openCreateBugForm.bind(this);
        this.onImageUpload = this.onImageUpload.bind(this);
        this.handleCreateFormSave = this.handleCreateFormSave.bind(this);
    }

    handleClick(e) {
        const newMarker = {
            id: `marker-${1 + Math.random()}`,
            position: e.latlng,
            title: '',
            type: 'new',
            images: [],
            creator: 'Michel Roca',
        };

        this.props.addOrUpdateMarker(newMarker.id, newMarker);

        this.setState({
            createFormOpened: true,
            currentMarker: newMarker,
            // markers: [...this.state.markers, newMarker],
        });
    }

    handleLocationFound(e) {
        this.setState({
            currentPosition: e.latlng,
            currentPositionAccuracy: e.accuracy,
        });
    }

    handleCreateFormSave() {
        const newMarker = Object.assign({}, this.state.currentMarker, this.state.currentMarkerCreation, { type: 'saved' });

        this.props.deleteMarkerByType('new');
        this.props.addOrUpdateMarker(newMarker.id, newMarker);

        this.setState({
            snackbarOpened: true,
            createFormOpened: false,
            createEventFormOpened: false,
            currentMarker: null,
            currentMarkerCreation: {},
        });
    }

    handleCreateFormClose() {
        this.props.deleteMarkerByType('new');
        this.setState({
            createFormOpened: false,
            createEventFormOpened: false,
            currentMarker: null,
            currentMarkerCreation: {},
        });
    }

    openCreateForm() {
        this.setState({
            createFormOpened: false,
            createEventFormOpened: true,
            currentMarkerCreation: Object.assign(this.state.currentMarkerCreation, { type: 'event' }),
        });
    }

    openCreateBugForm() {
        this.setState({
            createFormOpened: false,
            createEventFormOpened: true,
            currentMarkerCreation: Object.assign(this.state.currentMarkerCreation, { type: 'bug' }),
        });
    }

    onImageUpload(e) {
        this.setState({
            currentMarkerCreation: Object.assign({}, this.state.currentMarkerCreation, { image: URL.createObjectURL(e.target.files[0]) }),
        });
    }

    render() {
        return (
            <div style={{ height: '100%' }}>
                <BaseMap
                    center={this.state.center}
                    zoom={this.state.zoom}
                    style={{ height: '100%' }}
                    onClick={this.handleClick}
                    onLocationfound={this.handleLocationFound}
                    ref={this.map}
                >
                    <TileLayer
                        attribution="Map data © OpenStreetMap contributors"
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <MarkersList markers={this.props.markers} />
                    {this.state.currentPosition.length && <Circle center={this.state.currentPosition} fillColor="blue" radius={this.state.currentPositionAccuracy / 2} />}
                </BaseMap>

                <Dialog
                    modal={false}
                    open={this.state.createFormOpened}
                    onRequestClose={this.handleCreateFormClose}
                    autoScrollBodyContent
                >
                    <div style={{ textAlign: 'center' }}>
                        <RaisedButton label="Créer une opération coup de main" onClick={this.openCreateForm} primary fullWidth buttonStyle={{ height: 'auto' }} overlayStyle={{ height: 'auto' }} />
                        <br />
                        <br />
                        <RaisedButton label="Signaler une anomalie" onClick={this.openCreateBugForm} secondary fullWidth buttonStyle={{ height: 'auto' }} overlayStyle={{ height: 'auto' }} />
                    </div>
                </Dialog>

                <Dialog
                    modal={false}
                    open={this.state.createEventFormOpened}
                    onRequestClose={this.handleCreateFormClose}
                    autoScrollBodyContent
                >
                    {this.state.currentMarker &&
                    <div>
                        {this.state.currentMarkerCreation.type === 'event' &&
                        <span>Demander un coup de main</span>
                        }
                        {this.state.currentMarkerCreation.type === 'bug' &&
                        <span>Signaler un problème</span>
                        }

                        <TextField floatingLabelText="Titre" value={this.state.currentMarkerCreation.title} fullWidth />
                        <TextField floatingLabelText="Description" value={this.state.currentMarkerCreation.description} fullWidth multiLine rows={2} />

                        {this.state.currentMarkerCreation.type === 'event' &&
                        <div>
                            <DatePicker hintText="Date" autoOk />
                            <TimePicker format="24hr" hintText="Heure" autoOk />
                        </div>
                        }

                        <input type="file" onChange={this.onImageUpload} />
                        {this.state.currentMarkerCreation.image &&
                        <img src={this.state.currentMarkerCreation.image} alt="" style={{ maxWidth: '80%' }} />
                        }
                        <br />
                        <br />
                        <RaisedButton label="Faire la demande" onClick={this.handleCreateFormSave} primary fullWidth />
                    </div>
                    }
                </Dialog>

                <Snackbar
                    open={this.state.snackbarOpened}
                    message="Demande enregistrée !"
                    autoHideDuration={3000}
                />
            </div>
        );
    }
}

Map.propTypes = {
    markers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string,
        description: PropTypes.string,
        type: PropTypes.string.isRequired,
        images: PropTypes.arrayOf(PropTypes.shape({
            url: PropTypes.string,
        })),
    })).isRequired,
    addOrUpdateMarker: PropTypes.func.isRequired,
    deleteMarkerByType: PropTypes.func.isRequired,
};

export default Map;

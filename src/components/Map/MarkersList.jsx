import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MarkerPopup from './MarkerPopup';

class MarkersList extends Component {
    render() {
        const items = this.props.markers.map(marker => (
            <MarkerPopup key={marker.id} marker={marker} />
        ));

        return <div style={{ display: 'none' }}>{items}</div>;
    }
}

MarkersList.propTypes = {
    markers: PropTypes.arrayOf(PropTypes.shape).isRequired,
};

export default MarkersList;

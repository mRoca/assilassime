import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog, List, ListItem, Subheader } from 'material-ui';
import { white } from '../../helpers/style';
import Event from './Event';

class EventsList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentEvent: null,
        };

        this.handleEventSelect = this.handleEventSelect.bind(this);
        this.handlePopupClose = this.handlePopupClose.bind(this);
    }

    handleEventSelect(event) {
        this.setState({
            currentEvent: event,
        });
    }

    handlePopupClose() {
        this.setState({
            currentEvent: null,
        });
    }

    render() {
        const iconsTypes = {
            new: 'marker-current',
            saved: 'marker-current',
            town: 'marker-town',
            bug: 'marker-bug',
            event: 'marker',
        };

        return (
            <div>
                <Subheader><strong>Les coups de main demandés</strong></Subheader>

                <List style={{ backgroundColor: white }}>
                    {this.props.events.filter(event => event.type !== 'bug').map(event => (
                        <ListItem
                            key={event.id}
                            primaryText={event.title || 'En attente de validation'}
                            onClick={() => this.handleEventSelect(event)}
                            secondaryText={`${event.date ? `${event.date} à ${event.time} -` : ''} à ${Math.floor(100 * Math.random())}m`}
                            leftIcon={<img src={`images/${iconsTypes[event.type]}.svg`} alt="" />}
                        />
                    ))}
                </List>

                <Subheader><strong>Les problèmes signalés</strong></Subheader>

                <List style={{ backgroundColor: white }}>
                    {this.props.events.filter(event => event.type === 'bug').map(event => (
                        <ListItem
                            key={event.id}
                            primaryText={event.title}
                            onClick={() => this.handleEventSelect(event)}
                            secondaryText={`à ${Math.floor(100 * Math.random())}m`}
                            leftIcon={<img src={`images/${iconsTypes[event.type]}.svg`} alt="" />}
                        />
                    ))}
                </List>

                {this.state.currentEvent !== null &&
                <Dialog
                    modal={false}
                    open={this.state.currentEvent !== null}
                    onRequestClose={this.handlePopupClose}
                    autoScrollBodyContent
                >
                    <Event event={this.state.currentEvent} />
                </Dialog>
                }
            </div>
        );
    }
}

EventsList.propTypes = {
    events: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        date: PropTypes.string,
        time: PropTypes.string,
        type: PropTypes.string,
    })).isRequired,
};

export default EventsList;

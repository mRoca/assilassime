import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Avatar, List, ListItem, Divider, RaisedButton, GridTile, GridList } from 'material-ui';
import { ActionDone, CommunicationChatBubble, SocialShare } from 'material-ui/svg-icons/index';
import { PropTypes as MapPropTypes } from 'react-leaflet';

class Event extends Component {
    render() {
        return (
            <div>
                <h3>{this.props.event.title}</h3>

                <strong>Créateur : </strong>
                <span>{this.props.event.creator}</span>

                {this.props.event.date &&
                <div>
                    <br />
                    <Divider />
                    <br />

                    <strong>Date : </strong>
                    <span>{this.props.event.date} à {this.props.event.time}</span>
                </div>
                }

                <br /><br />
                <Divider />
                <br />

                <strong>Description</strong>
                <br />

                {this.props.event.type === 'saved' &&
                <div>
                    <p>
                        Cette fiche est en attente de validation par les administrateurs. Merci pour votre participation !
                    </p>
                </div>
                }

                {this.props.event.type === 'bug' &&
                <div>
                    <p>
                        Bonjour, plusieurs trous ont fait leur apparition dans la chaussée, sur le carrefour.
                    </p>
                </div>
                }

                {this.props.event.type === 'town' &&
                <div>
                    <p>
                        La mairie intervient ici pour réparer un lampadaire.
                    </p>
                </div>
                }

                {this.props.event.type === 'event' &&
                <div>
                    <p>
                        Bonjour, nous avons besoin d'aide pour reboucher quelques trous dans la chaussée.
                        <br />
                        <br />
                        Tous les bras sont les bienvenus, je me charge d'apporter quelques boissons.
                    </p>

                    <br />
                    <Divider />
                    <br />
                    <strong>Participants</strong>
                    <br />

                    <List>
                        <ListItem
                            primaryText="Brendan Lim"
                            leftAvatar={<Avatar src="images/person.svg" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                        <ListItem
                            primaryText="Eric Hoffman"
                            leftAvatar={<Avatar src="images/person.svg" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                        <ListItem
                            primaryText="Grace Ng"
                            leftAvatar={<Avatar src="images/person.svg" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                        <ListItem
                            primaryText="Kerem Suer"
                            leftAvatar={<Avatar src="images/person.svg" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                        <ListItem
                            primaryText="Raquel Parrado"
                            leftAvatar={<Avatar src="images/person.svg" />}
                            rightIcon={<CommunicationChatBubble />}
                        />
                    </List>
                </div>
                }

                {this.props.event.images.length > 0 &&
                <div>
                    <Divider />
                    <br />
                    <strong>Photos</strong>
                    <br />
                    <br />

                    <GridList cols={1}>
                        {this.props.event.images.map(image => (
                            <GridTile key={image.url} title={image.title}>
                                <img src={image.url} alt="" style={{ maxWidth: 500 }} />
                            </GridTile>
                        ))}
                    </GridList>
                </div>
                }

                <br />
                {this.props.event.type === 'bug' &&
                <div>
                    <RaisedButton label="Plannifier un coup main !" primary fullWidth buttonStyle={{ height: 'auto' }} overlayStyle={{ height: 'auto' }} />
                </div>
                }

                {this.props.event.type === 'event' &&
                <div>
                    <RaisedButton label="Je participe !" icon={<ActionDone />} primary fullWidth />
                </div>
                }

                <br /><br />
                <div>
                    <RaisedButton label="Je partage !" icon={<SocialShare />} secondary fullWidth />
                </div>
            </div>
        );
    }
}

Event.propTypes = {
    event: PropTypes.shape({
        type: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        position: MapPropTypes.latlng.isRequired,
        title: PropTypes.string,
        date: PropTypes.string,
        time: PropTypes.string,
        creator: PropTypes.string,
        description: PropTypes.string,
        children: MapPropTypes.children,
        images: PropTypes.arrayOf(PropTypes.shape({
            url: PropTypes.string,
        })),
    }).isRequired,
};

export default Event;

# This Dockerfile is here to be able to build a prod image without other step "docker build ."

FROM node:10.5-alpine

WORKDIR /srv

RUN apk add --no-cache git && yarn global add serve && yarn cache clean

COPY public /srv/public
COPY src /srv/src
COPY package.json yarn.lock /srv/

RUN yarn install --frozen-lockfile --production && npm run-script build && rm -rf ./node_modules && yarn cache clean

CMD serve -l tcp://0.0.0.0:5000 -s ./build/

EXPOSE 5000

HEALTHCHECK --interval=30s --timeout=1s CMD nc -z 0 5000
STOPSIGNAL SIGQUIT
